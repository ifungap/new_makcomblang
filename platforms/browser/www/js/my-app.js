var $$ = Dom7;
var server = "http://192.168.100.14:8000/makcomblang/makcomblang_server"; // PC
// var server = "http://ifta2015.com/ifung"; // Hosting
// var server = "http://localhost/makcomblang_server"; // Laptop
var app = new Framework7({
  // App root element
  root: '#app',
  // App Name
  name: 'Makcomblang App',
  // App id
  id: 'com.makcomblang.dev',
  // Enable swipe panel
  panel: {
    swipe: 'left',
    leftBreakpoint: 768,
  },
  theme: 'md',
  // Add default routes
  routes: [
    {
      path: '/index/',
      url: 'index.html',
      on: {
        pageInit: function(e,page){
          // mainView.router.refreshPage();
          app.panel.disableSwipe();
          if (localStorage.user_username){
            if (localStorage.user_status == 1){
              if (localStorage.user_newuser == 0){
                page.router.navigate('/create_data_profile/');
              }
              else{
                page.router.navigate('/client_home/');
              }
            }
            else{
              page.router.navigate('/makcomblang_home/');
            }
          }

          // Tombol home
          $$("#link-home-panel").on('click', function(){
            if (localStorage.user_status == 1){
              page.router.navigate('/client_home/');
            }
            else{
              page.router.navigate('/makcomblang_home/');
            }
          })

          // Tombol logout
          $$("#btn-log-out").on('click', function(){
            localStorage.removeItem("user_username");
            localStorage.removeItem("user_name");
            localStorage.removeItem("user_work");
            localStorage.removeItem("user_email");
            localStorage.removeItem("user_gender");
            localStorage.removeItem("user_location");
            localStorage.removeItem("user_urlimage");
            localStorage.removeItem("user_birthdate");
            localStorage.removeItem("user_phonenumber");
            localStorage.removeItem("user_description");
            localStorage.removeItem("user_id");
            localStorage.removeItem("user_newuser");
            localStorage.removeItem("user_status");
            page.router.navigate('/login/');
          })
        }
      }
    },
    {
      path: '/login/',
      url: 'login.html',
    },
    {
      path: '/register/',
      url: 'register.html',
    },
    {
      path: '/client_home/',
      url: 'client_home.html',
    },
    {
      path: '/makcomblang_home/',
      url: 'makcomblang_home.html',
    },
    {
      path: '/chat/',
      url: 'chat.html',
    },
    {
      path: '/notification/',
      url: 'notification.html',
    },
    {
      path: '/request/',
      url: 'request.html',
    },
    {
      path: '/profile/',
      url: 'profile.html',
    },
    {
      path: '/makcomblangmu/',
      url: 'makcomblangmu.html',
    },
    {
      path: '/daftar_makcomblang/',
      url: 'daftar_makcomblang.html',
    },
    {
      path: '/detail_makcomblang/:id',
      url: 'detail_makcomblang.html',
    },
    {
      path: '/request_makcomblang/:id',
      url: 'request_makcomblang.html',
    },
    {
      path: '/daftar_client/',
      url: 'daftar_client.html',
    },
    {
      path: '/detail_client/:id',
      url: 'detail_client.html',
    },
    {
      path: '/detail_info_client/:id',
      url: 'detail_info_client.html',
    },
    {
      path: '/client_matching/:id',
      url: 'client_matching.html',
    },
    {
      path: '/create_data_profile/',
      url: 'create_data_profile.html',
    },
    {
      path: '/daftar_rekomendasi/',
      url: 'daftar_rekomendasi.html',
    },
    {
      path: '/decision_client/:recommendation_id',
      url: 'decision_client.html',
    },
    {
      path: '/review_makcomblang/:id',
      url: 'review_makcomblang.html',
    }
  ],
  // ... other parameters
});

var mainView = app.view.create('.view-main',{
    url: '/index/',
});

// $$("#page-index").css('background-color','#2196f3');


$$(document).on('page:init',function(e,page){

  // Login page
  if (page.name == "login") {
    app.panel.disableSwipe();
    $$("#btnsignin").on('click', function(){
      // app.dialog.alert($$("#username-login").val());
      app.request.post(server + "/user/login.php",
      { username: $$("#username-login").val(), password: $$("#password-login").val() },
      function(data){
          if(data == 'failed')
          {
            // ALERT
            app.dialog.alert("The username or password that you have entered is incorrect.");
          }
          else
          {
            var result = JSON.parse(data);
            for (var i = 0; i < result.length; i++){
              localStorage.setItem("user_username",result[i]['username']);
              localStorage.setItem("user_name",result[i]['name']);
              localStorage.setItem("user_birthdate",result[i]['birthdate']);
              localStorage.setItem("user_email",result[i]['email']);
              localStorage.setItem("user_gender",result[i]['gender']);
              localStorage.setItem("user_location",result[i]['location']);
              localStorage.setItem("user_work",result[i]['work']);
              localStorage.setItem("user_phonenumber",result[i]['phone_number']);
              localStorage.setItem("user_description",result[i]['description']);
              localStorage.setItem("user_urlimage",result[i]['url_image']);
              localStorage.setItem("user_id",result[i]['id']);
              localStorage.setItem("user_newuser",result[i]['new_user']);
              localStorage.setItem("user_status",result[i]['status']);
            }
            if (localStorage.user_status == 1){
              if (localStorage.user_newuser == 0){
                mainView.router.navigate('/create_data_profile/');
              }
              else{
                page.router.navigate('/client_home/');
                // mainView.router.navigate('/client_home/');
              }
            }
            else{
              page.router.navigate('/makcomblang_home/');
              // mainView.router.navigate('/makcomblang_home/');
            }
          }
      })
    })
  }

  // Register page
  if (page.name == "register") {
    app.panel.disableSwipe();
    $$("#btnregister").on('click', function(){
      var username = $$("#username-register").val();
      var email = $$("#email-register").val();
      var password = $$("#password-register").val();
      var retype_password = $$("#retype_password-register").val();
      var status = $$("#sebagai-register").val();

      if(username && email && password && retype_password && status != null){
        if(password == retype_password){
          app.request.post(server + "/user/register.php",
          { username: username, password: password, email: email, status: status},
          function(data){
            if(data == "success"){
              mainView.router.navigate('/login/');
              app.dialog.alert("Selamat anda telah terdaftar !");
            }
            else {
              app.dialog.alert("Kesalahan telah terjadi. Coba lagi beberapa saat.");
            }
          });
        }
        else{
          app.dialog.alert("Password dan Konfirmasi password.");
        }
      }
    })
  }

  // Client Home page
  if (page.name == "client_home") {
    // console.log(localStorage.user_urlimage);
    if (localStorage.user_urlimage){
      $$(".panel-profile-picture").attr('src', localStorage.user_urlimage);
    }
    else{
      $$(".panel-profile-picture").attr('src', server + "/user/img/default.jpeg");
    }
    $$(".panel-profile-username").html("<b>" + localStorage.user_username + "</b>");

  }

  // Makcomblang Home page
  if (page.name == "makcomblang_home") {
    // console.log(localStorage.user_urlimage);
    if (localStorage.user_urlimage){
      $$(".panel-profile-picture").attr('src', localStorage.user_urlimage);
    }
    else{
      $$(".panel-profile-picture").attr('src', server + "/user/img/default.jpeg");
    }
    $$(".panel-profile-username").html("<b>" + localStorage.user_username + "</b>");

  }

  // Daftar makcomblang page
  if (page.name == "daftar_makcomblang"){
    // List of Makcomblang
    app.request.get(server + "/user/list_makcomblang.php", {}, function(data){
      var result = JSON.parse(data);
      for (var i = 0; i < result.length; i++) {
        $$("#list-makcomblang").append(
          '<li>' +
            '<a class="item-link item-content" href="/detail_makcomblang/' + result[i]['id'] + '">' +
              '<div class="item-media"><img src="'+ result[i]['url_image'] +'" width="44" height="44"/></div>' +
              '<div class="item-inner">' +
                '<div class="item-title-row">' +
                  '<div class="item-title"><b>' + result[i]['username'] + '</b></div>' +
                '</div>' +
                '<div class="item-subtitle">' + result[i]['location'] + '</div>' +
              '</div>' +
            '</a>' +
          '</li>'
        )
      }

    })

    // Create searchbar
    var searchbar = app.searchbar.create({
      el: '.searchbar',
      searchContainer: '.list',
      searchIn: '.item-title',
      on: {
        search(sb, query, previousQuery) {
          console.log(query, previousQuery);
        }
      }
    });
  }

  // Detail makcomblang page
  if (page.name == "detail_makcomblang"){
    var user_id = page.router.currentRoute.params.id;
    app.request.post(server + "/user/detail_makcomblang.php", { id: user_id }, function(data){
      if (data != "failed"){
        var result = JSON.parse(data);
        $$("#detail-makcomblang-title").html(result[0]['username'] + " Profile");
        $$(".detail-image-makcomblang").attr('src', result[0]['url_image']);
        var name = result[0]['username'];
        var kelamin = "-";
        var lokasi = "-";
        if (result[0]['name'].length > 0){
          name = result[0]['name'];
        }
        if (result[0]['gender'].length > 0){
          kelamin = result[0]['gender'];
        }
        if (result[0]['location'].length > 0){
          lokasi = result[0]['location'];
        }
        $$("#detail-header-makcomblang-nama").html(name);
        $$("#detail-makcomblang-nama").html(name);
        $$("#detail-makcomblang-email").html(result[0]['email']);
        $$("#detail-makcomblang-kelamin").html(kelamin);
        $$("#detail-makcomblang-kota").html(lokasi);

        var deskripsi = "Tidak ada deskripsi";
        if (result[0]['description'].length > 0){
          deskripsi = result[0]['description'];
        }
        $$("#detail-makcomblang-deskripsi").text(deskripsi);
        $$("#btn-request-makcomblang").on('click', function(){
          if (localStorage.user_status == 1) {
            mainView.router.navigate('/request_makcomblang/' + user_id);
          }
          else{
            app.dialog.alert("Fitur menambahkan makcomblang belum tersedia");
          }
        })
      }
    })
  }

  // Request makcomblang page
  if (page.name == "request_makcomblang"){
    var makcomblang_id = page.router.currentRoute.params.id;
    app.request.get(server + "/transaction/question.php", {}, function(data){
      localStorage.setItem("total_question", 0);
      var result = JSON.parse(data);
      var total_jawab = result.length;
      $$("#btn-request-next").text("Selanjutnya (" + total_jawab + ")");
      $$("#text-question").html(result[localStorage.total_question]['value']);
      for (var j = 0; j < result[localStorage.total_question]['answer'].length; j++) {
        $$("#text-answer").append(
          '<li>' +
            '<label class="item-radio item-content">' +
              '<input type="radio" name="demo-radio" id="answer" value="'+ result[localStorage.total_question]['answer'][j]['id'] +'" />'+
              '<i class="icon icon-radio"></i>' +
              '<div class="item-inner">' +
                '<div class="item-title">' + result[localStorage.total_question]['answer'][j]['value'] + '</div>' +
              '</div>' +
            '</label>' +
          '</li>'
        );
      }
      $$("#btn-request-next").on('click', function(){
        var radios_answer = document.getElementsByName('demo-radio');
        var radios_priority = document.getElementsByName('priority-radio');
        var radios_answer_value;
        var radios_priority_value;
        for (var i = 0, length = radios_answer.length; i < length; i++)
        {
         if (radios_answer[i].checked)
         {
          radios_answer_value = radios_answer[i].value;
          break;
         }
        }
        for (var i = 0, length = radios_priority.length; i < length; i++)
        {
         if (radios_priority[i].checked)
         {
          radios_priority_value = radios_priority[i].value;
          break;
         }
        }
        app.request.post(server + "/transaction/get_answer.php", {question_id: result[localStorage.total_question]['id'], user_id: localStorage.user_id, answer_id: radios_answer_value, priority: radios_priority_value}, function(data){
          if (data == "success") {
            if (total_jawab <= 0) {
              total_jawab = 0;
            }
            else{
              total_jawab --;
            }
            $$("#text-question").html("");
            $$("#text-answer").html("");
            $$("#btn-request-next").text("Selanjutnya (" + total_jawab + ")");
            localStorage.total_question ++;
            if (localStorage.total_question < result.length){
              $$("#text-question").html(result[localStorage.total_question]['value']);
              for (var j = 0; j < result[localStorage.total_question]['answer'].length; j++) {
                $$("#text-answer").append(
                  '<li>' +
                    '<label class="item-radio item-content">' +
                      '<input type="radio" name="demo-radio" value="' + result[localStorage.total_question]['answer'][j]['id'] + '" />'+
                      '<i class="icon icon-radio"></i>' +
                      '<div class="item-inner">' +
                        '<div class="item-title">' + result[localStorage.total_question]['answer'][j]['value'] + '</div>' +
                      '</div>' +
                    '</label>' +
                  '</li>'
                );
              }
              // console.log("Ke " + localStorage.total_question);
            }
            else{
              app.request.post(server + "/transaction/post_transaction.php",
                {makcomblang_id: makcomblang_id, user_id: localStorage.user_id, username: localStorage.user_username},function(data){
                  if (data == "success") {
                    app.dialog.alert("Berhasil mengirim permintaan");
                    localStorage.removeItem("total_question");
                    mainView.router.navigate('/client_home/');
                  }
                })

            }
          }
          else{
            app.dialog.alert(data);
          }
        })
      })
    })
  }

  // Create data profile page
  if (page.name == "create_data_profile") {
    // console.log(localStorage.user_id);
    // $$('#upload-photo').attr('src', server + "/user/img/default.jpeg");
    $$("#btn-get-photo").on('click', function(){
      navigator.camera.getPicture( onSuccess, onFail, {
        quality: 100,
  		  destinationType: Camera.DestinationType.DATA_URL,
  		  sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
  		  encodingType: Camera.EncodingType.JPEG,
  		  mediaType: Camera.MediaType.PICTURE,
  		  correctOrientation: true
  		});
      function onSuccess(imageData) {
        // $$("#text-photo").hide();
        $$('#upload-photo').attr('src', "data:image/jpeg;base64," + imageData);
      }
      function onFail(message) {
        app.dialog.alert('Pastikan gambar berformat .jpeg');
      }
    })
    $$("#btn-create-profile").on('click', function(){
      // General info
      var id = localStorage.user_id;
      var nama = $$("#nama").val();
      var jenis_kelamin = $$("#jenis_kelamin").val();
      var tanggal_lahir = $$("#tanggal_lahir").val();
      var nomor_ponsel = $$("#nomor_ponsel").val();
      var kota = $$("#kota").val();
      var bekerja = $$("#bekerja").val();
      var deskripsi = $$("#deskripsi").val();
      // app.dialog.alert(id);
      // Image info
      var imgUri = $$('#upload-photo').attr('src');
      if (imgUri){
        var options = new FileUploadOptions();
        options.fileKey = "photo";
        options.fileName = imgUri.substr(imgUri.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        options.params = { gender: jenis_kelamin, name: nama, birthdate: tanggal_lahir, phone_number: nomor_ponsel, location: kota, work: bekerja, description: deskripsi, user_id: id };
        var ft = new FileTransfer();
        ft.upload(imgUri, encodeURI(server + "/user/add_data_profile.php"), onUploadSuccess, onUploadError, options);
      }
      else{
        app.request.post(server + "/user/add_data_profile.php",
        { gender: jenis_kelamin, name: nama, birthdate: tanggal_lahir, phone_number: nomor_ponsel, location: kota, work: bekerja, description: deskripsi, user_id: id },
        function(data){
          if (data == "success"){
            localStorage.user_newuser = 1;
            mainView.router.navigate('/client_home/');
          }
        })
      }
    });

    $$("#btn-skip-profile").on('click', function(){
      if(localStorage.user_status == 1){
        mainView.router.navigate('/client_home/');
      }
      else{
        mainView.router.navigate('/makcomblang_home/');
      }
    })
  }

  // Request page
  if (page.name == "request") {
    app.request.post(server + "/transaction/get_pending_transaction.php",
    {makcomblang_id: localStorage.user_id},function(data){
      if (data == "failed"){
        $$("#request-list").append(
          "<div class='block'><li>Tidak ada request yang masuk</li></div>"
        );
      }
      else{
        var result = JSON.parse(data);
        for (var i = 0; i < result.length; i++) {
          $$("#request-list").append(
            '<li class="swipeout">' +
              '<div class="item-content swipeout-content">' +
                '<div class="item-media">' +
                  '<img src="' + result[i]['url_image']  + '" width="60" height="60" style="border-radius: 50%;"/>' +
                '</div>' +
                '<div class="item-inner">' +
                  '<div class="item-subtitle">Client ' + result[i]['client_username'] + ' mengirimi anda permintaan</div>' +
                '</div>' +
              '</div>' +
              '<div class="swipeout-actions-right">' +
                '<a href="#" class="color-green alert-reply swipeout-delete" onclick="decisionMakcomblang(' + result[i]['id'] + ", " + 1 + ')">Terima</a>' +
                '<a href="#" class="color-red alert-forward swipeout-delete" onclick="decisionMakcomblang(' + result[i]['id'] + "," + 2 +  ')">Tolak</a>' +
                '<a href="#" class="color-blue alert-reply" onclick="detailClient(' + result[i]['user_id'] + ", " + 1 + ')">Detail</a>' +
              '</div>' +
            '</li>'
          )
        }
      }
    })
  }

  // Detail client page
  if (page.name == "detail_client") {
    var user_id = page.router.currentRoute.params.id;
    // app.dialog.alert(user_id);
    app.request.post(server + "/user/detail_user.php", { id: user_id }, function(data){
      if (data != "failed"){
        var result = JSON.parse(data);
        $$("#detail-makcomblang-title").html(result[0]['username'] + " Profile");
        $$(".detail-image-user").attr('src', result[0]['url_image']);
        var name = result[0]['username'];
        var kelamin = "-";
        var lokasi = "-";
        if (result[0]['name'].length > 0){
          name = result[0]['name'];
        }
        if (result[0]['gender'].length > 0){
          kelamin = result[0]['gender'];
        }
        if (result[0]['location'].length > 0){
          lokasi = result[0]['location'];
        }
        $$("#detail-header-makcomblang-nama").html(name);
        $$("#detail-makcomblang-nama").html(name);
        $$("#detail-makcomblang-email").html(result[0]['email']);
        $$("#detail-makcomblang-kelamin").html(kelamin);
        $$("#detail-makcomblang-kota").html(lokasi);

        var deskripsi = "Tidak ada deskripsi";
        if (result[0]['description'].length > 0){
          deskripsi = result[0]['description'];
        }
        $$("#detail-makcomblang-deskripsi").text(deskripsi);
        $$("#btn-comblangkan").on('click', function(){
          mainView.router.navigate('/client_matching/' + user_id);
        })
      }
    })
  }

  // Detail client page
  if (page.name == "detail_info_client") {
    var user_id = page.router.currentRoute.params.id;
    // app.dialog.alert(user_id);
    app.request.post(server + "/user/detail_user.php", { id: user_id }, function(data){
      if (data != "failed"){
        var result = JSON.parse(data);
        $$("#detail-makcomblang-title").html(result[0]['username'] + " Profile");
        $$(".detail-image-user").attr('src', result[0]['url_image']);
        var name = result[0]['username'];
        var kelamin = "-";
        var lokasi = "-";
        if (result[0]['name'].length > 0){
          name = result[0]['name'];
        }
        if (result[0]['gender'].length > 0){
          kelamin = result[0]['gender'];
        }
        if (result[0]['location'].length > 0){
          lokasi = result[0]['location'];
        }
        $$("#detail-header-makcomblang-nama").html(name);
        $$("#detail-makcomblang-nama").html(name);
        $$("#detail-makcomblang-email").html(result[0]['email']);
        $$("#detail-makcomblang-kelamin").html(kelamin);
        $$("#detail-makcomblang-kota").html(lokasi);

        var deskripsi = "Tidak ada deskripsi";
        if (result[0]['description'].length > 0){
          deskripsi = result[0]['description'];
        }
        $$("#detail-makcomblang-deskripsi").text(deskripsi);
      }
    })
  }

  // Daftar client page
  if (page.name == "daftar_client"){
    $$("#list-client").html("");
    // List of Makcomblang
    app.request.post(server + "/user/list_client.php", {makcomblang_id: localStorage.user_id}, function(data){
      // app.dialog.alert(data);
      var result = JSON.parse(data);
      for (var i = 0; i < result.length; i++) {
        $$("#list-client").append(
          '<li>' +
            '<a class="item-link item-content" href="/detail_client/' + result[i]['id'] + '">' +
              '<div class="item-media"><img src="'+ result[i]['url_image'] +'" width="44" height="44"/></div>' +
              '<div class="item-inner">' +
                '<div class="item-title-row">' +
                  '<div class="item-title"><b>' + result[i]['username'] + '</b></div>' +
                '</div>' +
                '<div class="item-subtitle">' + result[i]['location'] + '</div>' +
              '</div>' +
            '</a>' +
          '</li>'
        )
      }

    })

    // Create searchbar
    var searchbar = app.searchbar.create({
      el: '.searchbar',
      searchContainer: '.list',
      searchIn: '.item-title',
      on: {
        search(sb, query, previousQuery) {
          console.log(query, previousQuery);
        }
      }
    });
  }

  // CLient Matchig page
  if (page.name == "client_matching"){
    $$("#list-matching").html("");
    var user_id = page.router.currentRoute.params.id;
    app.request.post(server + "/transaction/user_matching.php",
    {user_id: user_id, makcomblang_id: localStorage.user_id},function(data){
      var result = JSON.parse(data);
      for (var i = 0; i < result.length; i++) {
        $$("#list-matching").append(
          '<li class="swipeout">' +
            '<div class="item-content swipeout-content">' +
              '<div class="item-media">' +
                '<img src="' + result[i]['url_image']  + '" width="60" height="60" style="border-radius: 50%;"/>' +
              '</div>' +
              '<div class="item-inner">' +
                '<div class="item-subtitle">' + result[i]['username'] + ' ( <i class="f7-icons heart-icon">heart</i> ' + result[i]['percentage'] +'% )</div>' +
              '</div>' +
            '</div>' +
            '<div class="swipeout-actions-right">' +
              '<a href="#" class="color-green alert-reply" onclick="comablangkanClient(' + result[i]['id'] + ", " + user_id + ')">Comblangkan</a>' +
              '<a href="#" class="color-blue alert-reply" onclick="detailClient(' + result[i]['id'] + ", " + 2 + ')">Detail</a>' +
            '</div>' +
          '</li>'
        )
      }
    })

    // Create searchbar
    var searchbar = app.searchbar.create({
      el: '.searchbar',
      searchContainer: '.list',
      searchIn: '.item-subtitle',
      on: {
        search(sb, query, previousQuery) {
          console.log(query, previousQuery);
        }
      }
    });
  }

  // Makcomblangmu page
  if (page.name == "makcomblangmu"){
    app.request.post(server + "/user/makcomblangmu.php", { client_id: localStorage.user_id }, function(data){
      if (data != "failed"){
        var result = JSON.parse(data);
        for (var i = 0; i < result.length; i++) {
          $$("#list-makcomblangmu").append(
            '<li class="swipeout">' +
              '<div class="item-content swipeout-content">' +
                '<div class="item-media">' +
                  '<img src="' + result[i]['url_image']  + '" width="60" height="60" style="border-radius: 50%;"/>' +
                '</div>' +
                '<div class="item-inner">' +
                  '<div class="item-subtitle">' + result[i]['username'] + '</div>' +
                '</div>' +
              '</div>' +
              '<div class="swipeout-actions-right">' +
              '<a href="#" class="color-green alert-reply" onclick="sendMessage(' + localStorage.user_id + ", " + result[i]['makcomblang_id'] + ')">Pesan</a>' +
              '<a href="/daftar_rekomendasi/" class="color-blue alert-reply">Cek Rekomendasi</a>' +
              '</div>' +
            '</li>'
          )
        }
      }
      else{
        $$("#list-makcomblangmu").append('<div class="block"><li>Anda belum memiliki makomblang</li></div>');
      }
    })
  }

  // Rekomendasi page
  if (page.name == "daftar_rekomendasi"){
    app.request.post(server + "/transaction/list_recommendation.php", { client_id: localStorage.user_id }, function(data){
      if (data != "failed"){
        var result = JSON.parse(data);
        for (var i = 0; i < result.length; i++) {
          console.log(result[i]['tmp_status']);
          if (result[i]['tmp_status'] == 0){
            $$("#list-recommendation").append(
              '<li class="swipeout">' +
                '<div class="item-content swipeout-content">' +
                  '<div class="item-media">' +
                    '<img src="' + result[i]['url_image']  + '" width="60" height="60" style="border-radius: 50%;"/>' +
                  '</div>' +
                  '<div class="item-inner">' +
                    '<div class="item-subtitle">' + result[i]['username'] + '</div>' +
                  '</div>' +
                '</div>' +
                '<div class="swipeout-actions-right">' +
                  '<a href="/detail_info_client/' + result[i]['recommendation_user_id'] + '" class="color-red">Detail</a>' +
                  '<a href="#" class="color-green alert-reply" onclick="sendMessage(' + localStorage.user_id + ", " + result[i]['recommendation_user_id'] + ')")">Pesan</a>' +
                  '<a href="/decision_client/'+ result[i]['recommendation_id'] +'" class="color-blue alert-reply">Keputusan</a>' +
                '</div>' +
              '</li>'
            )
          }
          else if (result[i]['tmp_status'] == 1){
            if (result[i]['user_on_decision'] == localStorage.user_id){
              $$("#note-recommendation").html('Menunggu konfirmasi dari ' + result[i]['username']);
              $$("#list-recommendation").append(
                '<li class="swipeout">' +
                  '<div class="item-content swipeout-content">' +
                    '<div class="item-media">' +
                      '<img src="' + result[i]['url_image']  + '" width="60" height="60" style="border-radius: 50%;"/>' +
                    '</div>' +
                    '<div class="item-inner">' +
                      '<div class="item-subtitle">' + result[i]['username'] + '</div>' +
                    '</div>' +
                  '</div>' +
                  '<div class="swipeout-actions-right">' +
                    '<a href="/detail_info_client/' + result[i]['recommendation_user_id'] + '" class="color-red">Detail</a>' +
                    '<a href="#" class="color-green alert-reply" onclick="sendMessage(' + localStorage.user_id + ", " + result[i]['recommendation_user_id'] + ')")">Pesan</a>' +
                  '</div>' +
                '</li>'
              )
            }
            else{
              $$("#daftar-recommendation").html("");
              $$("#daftar-recommendation").html(
                '<div class="title-content">Client ' + result[i]['username'] +' merasa cocok terhadap anda. Apakah anda juga cocok terhadap client ' + result[i]['username'] +' ?</div>' +
                '<div class="row">' +
                  '<a class="col button button-outline" href="#" onclick="decisionClient(' + result[i]['recommendation_id'] + ", " + 1 +')">Ya</a>' +
                  '<a class="col button button-outline" href="#" onclick="decisionClient(' + result[i]['recommendation_id'] + ", " + 2 +')">Tidak</a>' +
                '</div>' +
                '<div class="notice-content need-margin">Catatan : Jika anda tidak cocok / ragu terhadap pilihan yang telah diberikan Makcomblang maka Makcomblang bisa mencarikan pasangan lagi yang lebih cocok untuk anda.</div>'
              );
            }
          }
          else if (result[i]['tmp_status'] == 2){
            $$("#daftar-recommendation").html("");
            if (result[i]['recommendation_status'] == 1){
              $$("#daftar-recommendation").html(
                'Selamat client ' + result[i]['username'] + ' juga merasa cocok dengan anda. Semoga hubungan anda dengan ' + result[i]['username'] + ' berjalan dengan baik.' +
                '<div class="row">' +
                  '<a class="col button button-big button-fill color-green" href="/review_makcomblang/' + result[i]['makcomblang_id'] +'">Review Makcomblang</a>' +
                '</div>'
              );
            }
          }
        }
      }
      else{
        $$("#list-recommendation").append('<div class="block"><li>Anda belum memiliki rekomendasi dari makcomblang</li></div>');
      }
    })
  }

  // Decision Client page
  if (page.name == "decision_client"){
    var recommendation_id = page.router.currentRoute.params.recommendation_id;
    $$("#decision_yes").on('click', function(){
      app.request.post(server + "/transaction/decision_client.php", { user_id: localStorage.user_id, recommendation_id: recommendation_id, decision: 1 }, function(data){
        if (data == "success"){
          app.dialog.alert("Anda telah membuat keputusan");
          mainView.router.navigate("/client_home/");
        }
        else{
          app.dialog.alert("Kesalahan telah terjadi. Coba lagi beberapa saat.");
        }
      })
    })
    $$("#decision_no").on('click', function(){
      app.request.post(server + "/transaction/decision_client.php", { user_id: localStorage.user_id, recommendation_id: recommendation_id, decision: 2 }, function(data){
        if (data == "success"){
          app.dialog.alert("Anda telah membuat keputusan");
          mainView.router.navigate("/client_home/");
        }
        else{
          app.dialog.alert("Kesalahan telah terjadi. Coba lagi beberapa saat.");
        }
      })
    })
    // app.dialog.alert(recommendation_id);
  }

  // Review makcomblang page
  if (page.name == "review_makcomblang"){
    var makcomblang_id = page.router.currentRoute.params.id;
    // app.dialog.alert(makcomblang_id);
    var rating = 0;
    $$('[type*="radio"]').change(function() {
      rating = this.value;
    });
    $$("#btn-review").on('click', function(){
      var review = $$("#review").val();
      // console.log(rating + " " + review);
      app.request.post(server + "/transaction/post_review.php", { makcomblang_id: makcomblang_id, client_id: localStorage.user_id, rating: rating, review: review }, function(data){
        if (data == "success"){
          app.dialog.alert("Anda telah berhasil memberikan rating");
          mainView.router.navigate("/client_home/");
        }
      })
    })
  }

  // Profile page
  if (page.name == "profile"){
    app.request.post(server + "/user/detail_user.php", { id: localStorage.user_id}, function(data){
      if (data != "failed"){
        var result = JSON.parse(data);
        for (var i = 0; i < result.length; i++) {
          $$("#profile-photo").attr('src', result[i]['url_image']);
          $$("#profile-nama").val(result[i]['name']);
          var gender = result[i]['gender'];
          if (gender == "Pria"){
            $$("#profile-jenis_kelamin").html(
              '<option value="Pria" selected>Pria</option>' +
              '<option value="Wanita">Wanita</option>'
            )
          }
          else if (gender == "Wanita"){
            $$("#profile-jenis_kelamin").html(
              '<option value="Pria">Pria</option>' +
              '<option value="Wanita" selected>Wanita</option>'
            )
          }
          $$("#profile-tanggal_lahir").val(result[i]['birthdate']);
          $$("#profile-nomor_ponsel").val(result[i]['phone_number']);

          var city = result[i]['location'];
          if (city == "Surabaya"){
            $$("#select-profile-kota").append(
              '<a class="item-link smart-select smart-select-init" >' +
              '<select name="kota" id="profile-kota">' +
              '<option value="Surabaya" selected>Surabaya</option>' +
              '<option value="Jakarta">Jakarta</option>' +
              '<option value="Bandung">Bandung</option>' +
              '<option value="Jogjakarta">Jogjakarta</option>' +
              '<option value="Semarang">Semarang</option>' +
              '<option value="Bogor">Bogor</option>' +
              '</select>' +
              '<div class="item-content">' +
                '<div class="item-inner">' +
                  '<div class="item-title">Kota Tinggal</div>' +
                '</div>' +
              '</div>' +
              '</a>'
            )
          }
          if (city == "Jakarta"){
            // $$("#profile-kota").val("Jakarta");
            $$("#select-profile-kota").append(
              '<a class="item-link smart-select smart-select-init" >' +
              '<select name="kota" id="profile-kota">' +
              '<option value="Surabaya">Surabaya</option>' +
              '<option value="Jakarta" selected>Jakarta</option>' +
              '<option value="Bandung">Bandung</option>' +
              '<option value="Jogjakarta">Jogjakarta</option>' +
              '<option value="Semarang">Semarang</option>' +
              '<option value="Bogor">Bogor</option>' +
              '</select>' +
              '<div class="item-content">' +
                '<div class="item-inner">' +
                  '<div class="item-title">Kota Tinggal</div>' +
                '</div>' +
              '</div>' +
              '</a>'
            )
          }
          if (city == "Bandung"){
            $$("#select-profile-kota").append(
              '<a class="item-link smart-select smart-select-init" >' +
              '<select name="kota" id="profile-kota">' +
              '<option value="Surabaya">Surabaya</option>' +
              '<option value="Jakarta" >Jakarta</option>' +
              '<option value="Bandung" selected>Bandung</option>' +
              '<option value="Jogjakarta">Jogjakarta</option>' +
              '<option value="Semarang">Semarang</option>' +
              '<option value="Bogor">Bogor</option>' +
              '</select>' +
              '<div class="item-content">' +
                '<div class="item-inner">' +
                  '<div class="item-title">Kota Tinggal</div>' +
                '</div>' +
              '</div>' +
              '</a>'
            )
          }
          if (city == "Jogjakarta"){
            $$("#select-profile-kota").append(
              '<a class="item-link smart-select smart-select-init" >' +
              '<select name="kota" id="profile-kota">' +
              '<option value="Surabaya">Surabaya</option>' +
              '<option value="Jakarta" >Jakarta</option>' +
              '<option value="Bandung" >Bandung</option>' +
              '<option value="Jogjakarta" selected>Jogjakarta</option>' +
              '<option value="Semarang">Semarang</option>' +
              '<option value="Bogor">Bogor</option>' +
              '</select>' +
              '<div class="item-content">' +
                '<div class="item-inner">' +
                  '<div class="item-title">Kota Tinggal</div>' +
                '</div>' +
              '</div>' +
              '</a>'
            )
          }
          if (city == "Semarang"){
            $$("#select-profile-kota").append(
              '<a class="item-link smart-select smart-select-init" >' +
              '<select name="kota" id="profile-kota">' +
              '<option value="Surabaya">Surabaya</option>' +
              '<option value="Jakarta" >Jakarta</option>' +
              '<option value="Bandung">Bandung</option>' +
              '<option value="Jogjakarta">Jogjakarta</option>' +
              '<option value="Semarang" selected>Semarang</option>' +
              '<option value="Bogor">Bogor</option>' +
              '</select>' +
              '<div class="item-content">' +
                '<div class="item-inner">' +
                  '<div class="item-title">Kota Tinggal</div>' +
                '</div>' +
              '</div>' +
              '</a>'
            )
          }
          if (city == "Bogor"){
            $$("#select-profile-kota").append(
              '<a class="item-link smart-select smart-select-init" >' +
              '<select name="kota" id="profile-kota">' +
              '<option value="Surabaya">Surabaya</option>' +
              '<option value="Jakarta">Jakarta</option>' +
              '<option value="Bandung">Bandung</option>' +
              '<option value="Jogjakarta">Jogjakarta</option>' +
              '<option value="Semarang">Semarang</option>' +
              '<option value="Bogor" selected>Bogor</option>' +
              '</select>' +
              '<div class="item-content">' +
                '<div class="item-inner">' +
                  '<div class="item-title">Kota Tinggal</div>' +
                '</div>' +
              '</div>' +
              '</a>'
            )
          }

          var bekerja = result[i]['work'];
          if (bekerja == "ya"){
            $$("#select-profile-bekerja").append(
            '<a class="item-link smart-select smart-select-init" data-open-in="sheet">' +
              '<select name="mac-windows" id="profile-bekerja">' +
                '<option value="ya" selected>Ya</option>' +
                '<option value="tidak">Tidak</option>' +
              '</select>' +
              '<div class="item-content">' +
                '<div class="item-inner">' +
                  '<div class="item-title">Bekerja ?</div>' +
                '</div>' +
              '</div>' +
            '</a>'
            )
          }
          else{
            $$("#select-profile-bekerja").append(
            '<a class="item-link smart-select smart-select-init" data-open-in="sheet">' +
              '<select name="mac-windows" id="profile-bekerja">' +
                '<option value="ya">Ya</option>' +
                '<option value="tidak" selected>Tidak</option>' +
              '</select>' +
              '<div class="item-content">' +
                '<div class="item-inner">' +
                  '<div class="item-title">Bekerja ?</div>' +
                '</div>' +
              '</div>' +
            '</a>'
            )
          }

          $$("#profile-deskripsi").val(result[i]['description']);
        }
        $$("#btn-update-photo").on('click', function(){
          navigator.camera.getPicture( onSuccess, onFail, {
            quality: 100,
      		  destinationType: Camera.DestinationType.DATA_URL,
      		  sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      		  encodingType: Camera.EncodingType.JPEG,
      		  mediaType: Camera.MediaType.PICTURE,
      		  correctOrientation: true
      		});
          function onSuccess(imageData) {
            // $$("#text-photo").hide();
            $$('#profile-photo').attr('src', "data:image/jpeg;base64," + imageData);
          }
          function onFail(message) {
            app.dialog.alert('Pastikan gambar berformat .jpeg');
          }
        })
        $$("#btn-update-profile").on('click', function(){
          var imgUri = $$('#profile-photo').attr('src');
          if (imgUri){
            // app.dialog.alert(imgUri);
            var options = new FileUploadOptions();
            options.fileKey = "photo";
            options.fileName = imgUri.substr(imgUri.lastIndexOf('/') + 1);
            options.mimeType = "image/jpeg";
            options.params = { id: localStorage.user_id, gender: $$("#profile-jenis_kelamin").val(), name: $$("#profile-nama").val(), birthdate: $$("#profile-tanggal_lahir").val(), phone_number: $$("#profile-nomor_ponsel").val(), location: $$("profile-kota").val(), work: $$("profile-bekerja").val(), description: $$("profile-deskripsi").val() };
            var ft = new FileTransfer();
            ft.upload(imgUri, encodeURI(server + "/user/add_data_profile.php"), onUploadSuccess, onUploadError, options);
            app.dialog.alert($$("#profile-nama").val());
          }
          else{
            // app.dialog.alert("ee");
            app.request.post(server + "/user/add_data_profile.php",
            { id: localStorage.user_id, gender: $$("#profile-jenis_kelamin").val(), name: $$("#profile-nama").val(), birthdate: $$("#profile-tanggal_lahir").val(), phone_number: $$("#profile-nomor_ponsel").val(), location: $$("profile-kota").val(), work: $$("profile-bekerja").val(), description: $$("profile-deskripsi").val() },
            function(data){
              if (data == "success"){
                localStorage.user_newuser = 1;
                mainView.router.navigate('/client_home/');
              }
            })
          }
        })
      }
    })
  }
});

// Function
function onUploadSuccess(){
  localStorage.user_newuser = 1;
  localStorage.user_urlimage = server + "/user/img/" + localStorage.user_id + ".jpeg";
  // mainView.router.refreshPage();
  mainView.router.navigate('/index/');
}

function onUploadError(message){
  app.dialog.alert('Kesalahan telah terjadi. Coba lagi beberapa saat.');
}

function decisionMakcomblang(transaction_id, status){
  // app.dialog.alert(client_username);
  app.request.post(server + "/transaction/decision_transaction.php",
  {transaction_id: transaction_id, status: status},function(data){
    if (data == "failed"){
      app.dialog.alert("Kesalahan telah terjadi. Coba lagi beberapa saat.");
    }
    else{
      if (status == 1){
        app.dialog.alert("Client berhasil ditambahkan dalam daftar client");
      }
    }
  })
}

function detailClient(client_id, status){
  mainView.router.navigate('/detail_info_client/' + client_id);
}

function comablangkanClient(hasilComblang, userCombalng){
  app.dialog.confirm('Apakah anda yakin mencomblangkan dengan user ini?', function () {
    app.request.post(server + "/transaction/recommendation.php",
    {matching_user: userCombalng, recommendation_user: hasilComblang, makcomblang_id: localStorage.user_id},function(data){
      if (data == "success"){
        app.dialog.alert("Percomblangan sukses dilakukan");
        mainView.router.navigate("/makcomblang_home/");
      }
      else{
        app.dialog.alert("Kesalahan telah terjadi. Coba lagi beberapa saat.");
      }
    })
  });
}

function checkRecommendation(user_id){
  app.dialog.alert(user_id);
}

function sendMessage(sender, receiver){
  app.dialog.alert(sender + " " + receiver);
}

function decisionClient(recommendation_id, status){
  // app.dialog.alert(localStorage.user_id + " " + recommendation_id + " " + status);
  app.request.post(server + "/transaction/decision_client.php", { user_id: localStorage.user_id, recommendation_id: recommendation_id, decision: status }, function(data){
    if (data == "success"){
      app.dialog.alert("Anda telah membuat keputusan");
      mainView.router.navigate("/client_home/");
    }
    else{
      app.dialog.alert("Kesalahan telah terjadi. Coba lagi beberapa saat.");
    }
  })
}
